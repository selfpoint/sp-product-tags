(function (angular) {
    'use script';

    angular.module('spProductTags')
        .constant('PRODUCT_TAG_TYPES', {
            SIMPLE: 1,
            LANDING_PAGE: 2,
            EXPRESS_DELIVERY: 3,
            DELIVERY_WITHIN_DAYS: 4,
            EBT_ELIGIBLE: 5,
            EBT_CASH_ELIGIBLE: 6,
            HEAVY_PACKAGE: 7,
            COUPON: 8
        })
        .constant('PRODUCT_TAGS_CACHE_NAME', 'product-tags-cache')
        .run(['$rootScope', 'PRODUCT_TAG_TYPES', 'PRODUCT_TAGS_CACHE_NAME', function($rootScope, PRODUCT_TAG_TYPES, PRODUCT_TAGS_CACHE_NAME) {
            $rootScope.PRODUCT_TAG_TYPES = PRODUCT_TAG_TYPES;
            $rootScope.PRODUCT_TAGS_CACHE_NAME = PRODUCT_TAGS_CACHE_NAME;
        }]);
})(angular);