(function (angular) {
    'use strict';

    angular.module('spProductTags').filter('productTag', [
        '$filter', 'PRODUCT_TAGS_CACHE_NAME',
        function ($filter, PRODUCT_TAGS_CACHE_NAME) {
            return function (productTagId) {
                return $filter('spLocalCache')(productTagId, PRODUCT_TAGS_CACHE_NAME);
            };
        }
    ]);
})(angular);