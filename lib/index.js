(function (angular) {
    'use script';

    angular.module('spProductTags', ['spLocalCache'])
        .config(['SpLocalCacheProvider', function(SpLocalCacheProvider) {
            SpLocalCacheProvider.fetchData = [
                'SpProductTags', 'dataIds', 'cacheName', 'PRODUCT_TAGS_CACHE_NAME',
                function(SpProductTags, dataIds, cacheName, PRODUCT_TAGS_CACHE_NAME) {
                    if (cacheName !== PRODUCT_TAGS_CACHE_NAME) {
                        return;
                    }

                    return SpProductTags.getProductTags({
                        from: 0,
                        size: dataIds.length,
                        id: dataIds
                    }).then(function(resp) {
                        return resp.productTags;
                    });
                }
            ];
        }]);
})(angular);