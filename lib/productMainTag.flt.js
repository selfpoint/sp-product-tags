(function (angular) {
    'use strict';

    var SEARCH_IN_CHUNK_SIZE = 5;

    angular.module('spProductTags').filter('productMainTag', [
        '$filter', '$q', 'SpProductTags', 'PRODUCT_TAG_TYPES', 'PRODUCT_TAGS_CACHE_NAME',
        function ($filter, $q, SpProductTags, PRODUCT_TAG_TYPES, PRODUCT_TAGS_CACHE_NAME) {
            var tagIdByProductId = {};

            function _setProductMainTag(product, boostTags, mapKey) {
                if (!product.productTagsData) {
                    return $q.resolve();
                }

                var boostTagsMap = {};
                angular.forEach(boostTags, function(tagId) {
                    boostTagsMap[tagId] = true;
                });

                var productBoostedTags = [],
                    productAllTags = [];
                angular.forEach(product.productTagsData, function(productTag) {
                    if (!SpProductTags.isProductTagRelationActive(productTag) ||
                        productTag.showIconOnProductPageOnly ||
                        productTag.typeId === PRODUCT_TAG_TYPES.LANDING_PAGE) {
                        return;
                    }

                    if (boostTagsMap[productTag.id]) {
                        productBoostedTags.push(productTag.id);
                    } else {
                        productAllTags.push(productTag.id);
                    }
                });

                return _findMainTag(productBoostedTags.concat(productAllTags)).then(function(mainProductTag) {
                    if (mainProductTag) {
                        tagIdByProductId[mapKey] = mainProductTag.id;
                    }
                });
            }

            function _findMainTag(productTagIds) {
                if (!productTagIds.length) {
                    return $q.resolve();
                }

                // prepare promises for the next chunk to search in
                var promises = [];
                angular.forEach(productTagIds.splice(0, SEARCH_IN_CHUNK_SIZE), function(productTagId) {
                    promises.push(SpProductTags.getLocalProductTag(productTagId));
                });

                return $q.all(promises).then(function(productTags) {
                    // return the first product tag with an icon
                    for (var i = 0; i < productTags.length; i++) {
                        if (productTags[i] && _productTagHasAnIcon(productTags[i])) {
                            return productTags[i];
                        }
                    }

                    // if not found look for another one in the next chunk
                    return _findMainTag(productTagIds);
                });
            }

            function _productTagHasAnIcon(productTag) {
                var languageKeys = Object.keys(productTag.languages);
                for (var i = 0; i < languageKeys.length; i++) {
                    if (productTag.languages[languageKeys[i]].iconResourceUrl) {
                        return true;
                    }
                }
                return false;
            }

            function _getMapKey(product, boostTags) {
                var arr = [product.id];
                angular.forEach(boostTags, function(boostTag) {
                    if (boostTag) {
                        arr.push(boostTag);
                    }
                });
                return arr.join('-');
            }

            return function (product, boostTags) {
                var key = _getMapKey(product, boostTags);

                if (!tagIdByProductId.hasOwnProperty(key)) {
                    tagIdByProductId[key] = undefined;
                    _setProductMainTag(product, boostTags, key);
                    return;
                }

                if (tagIdByProductId[key]) {
                    return $filter('spLocalCache')(tagIdByProductId[key], PRODUCT_TAGS_CACHE_NAME);
                }
            };
        }
    ]);
})(angular);