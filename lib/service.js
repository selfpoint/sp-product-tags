(function (angular) {
    'use strict';

    angular.module('spProductTags').service('SpProductTags', [
        'Api', '$q', 'SpLocalCache', 'PRODUCT_TAGS_CACHE_NAME', 'PRODUCT_TAG_TYPES',
        function(Api, $q, SpLocalCache, PRODUCT_TAGS_CACHE_NAME, PRODUCT_TAG_TYPES) {
            var self = this,
                retailerIdDefer,
                retailerIdPromiseFulfilled = false,
                _deliveryWithinTagPromise;

            self.setRetailerId = setRetailerId;
            self.getRetailerId = getRetailerId;
            self.getProductTags = getProductTags;
            self.getDeliveryWithinDaysTag = getDeliveryWithinDaysTag;
            self.getLocalProductTag = getLocalProductTag;
            self.setLocalProductTag = setLocalProductTag;
            self.resetProductTagsCache = resetProductTagsCache;
            self.isProductTagRelationActive = isProductTagRelationActive;

            /**
             * Saves the retailer id on the service to be able to GET product tags
             * @public
             *
             * @param {number} retailerId
             *
             * @returns {Promise}
             */
            function setRetailerId(retailerId) {
                // when resetting the retailer id the retailer id promise should be reset
                if (retailerId === undefined) {
                    _resetRetailerIdPromise();
                    return $q.resolve();
                }

                return _resolveRetailerId(retailerId);
            }

            /**
             * Returns a promise which will be resolved when the retailer id will be set
             * @public
             *
             * @returns {Promise<number>}
             */
            function getRetailerId() {
                return _getRetailerIdDefer().promise;
            }

            /**
             * Returns a deferred promise for the retailer id
             * @private
             *
             * @returns {Deferred}
             */
            function _getRetailerIdDefer() {
                if (!retailerIdDefer) {
                    retailerIdDefer = $q.defer();
                }

                return retailerIdDefer;
            }

            /**
             * Resets the retailer id promise when it was fulfilled
             * @private
             */
            function _resetRetailerIdPromise() {
                if (retailerIdPromiseFulfilled) {
                    retailerIdPromiseFulfilled = false;
                    retailerIdDefer = undefined;
                    resetProductTagsCache();
                }
            }

            /**
             * Resolves the retailer id defer with the given retailer id
             * @private
             *
             * @param {number} retailerId
             *
             * @returns {Promise}
             */
            function _resolveRetailerId(retailerId) {
                return $q.resolve().then(function() {
                    if (!retailerIdPromiseFulfilled) {
                        return;
                    }

                    return _getRetailerIdDefer().promise;
                }).then(function(savedRetailerId) {
                    // reset the promise for the new retailer id to be returned from the promise in the future
                    if (savedRetailerId && savedRetailerId !== retailerId) {
                        _resetRetailerIdPromise();
                    }

                    retailerIdPromiseFulfilled = true;
                    _getRetailerIdDefer().resolve(retailerId);
                });
            }

            /**
             * Get product tags
             * @public
             *
             * @param {Object} params
             *
             * @returns {Promise<Object>}
             */
            function getProductTags(params) {
                return getRetailerId().then(function(retailerId) {
                    return Api.request({
                        method: 'GET',
                        url: '/v2/retailers/' + retailerId + '/product-tags',
                        params: params
                    }).then(function(res) {
                        angular.forEach(res.productTags, function(productTag) {
                            setLocalProductTag(productTag.id, productTag);
                        });

                        return res;
                    });
                });
            }

            /**
             * Will return the first product tag with delivery within days type
             * @public
             *
             * @return {Promise<Object>}
             */
            function getDeliveryWithinDaysTag() {
                if (_deliveryWithinTagPromise) {
                    return _deliveryWithinTagPromise;
                }

                return _deliveryWithinTagPromise = getProductTags({
                    from: 0,
                    size: 1,
                    productTagTypeId: PRODUCT_TAG_TYPES.DELIVERY_WITHIN_DAYS
                }).then(function(resp) {
                    return resp.productTags[0];
                }).catch(function() {
                    _deliveryWithinTagPromise = undefined;
                });
            }

            /**
             * Returns the local product tag from the local product tags cache
             * @public
             *
             * @param {number} productTagId
             * @param {Object} options
             *
             * @returns {Object}
             */
            function getLocalProductTag(productTagId, options) {
                return SpLocalCache.getLocalData(productTagId, PRODUCT_TAGS_CACHE_NAME, options)
            }

            /**
             * Sets a product tag to the local product tags cache
             * @public
             *
             * @param {number} productTagId
             * @param {Object} productTag
             */
            function setLocalProductTag(productTagId, productTag) {
                return SpLocalCache.setLocalData(productTagId, PRODUCT_TAGS_CACHE_NAME, productTag)
            }

            /**
             * Empties the product tags local cache
             * @public
             */
            function resetProductTagsCache() {
                SpLocalCache.resetLocalCache(PRODUCT_TAGS_CACHE_NAME);
            }

            /**
             * Returns whether a product tag data item on a product is active or not
             * @public
             *
             * @param {object} productTagRelation
             *
             * @returns {boolean}
             */
            function isProductTagRelationActive(productTagRelation) {
                return productTagRelation.isActive && (
                    productTagRelation.isRetailerRelation || productTagRelation.isGS1Relation ||
                    !productTagRelation.isGlobalRelation || !productTagRelation.isExcluded
                );
            }
        }
    ]);
})(angular);