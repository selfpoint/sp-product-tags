# sp-product-tags

An angularjs package to manage the product tags cache and frontend logic.

### In `index.html`

The package is dependent of the packages `sp-api` and `sp-local-cache`, they are dependencied in the bower file.
These files must be included in your site (besides the file of this package):
```
<script type="application/javascript" src="bower_components/sp-api/dist/sp-api.js"></script>
<script type="application/javascript" src="bower_components/sp-local-cache/dist/sp-local-cache.js"></script>
```

### Init

In order for the entire package to work, the user client must provide the retailer id on the first angularjs app run.
Example:
```
angular.module('MyApp', ['spProductTags']).run(['SpProductTags', function(SpProductTags) {
    SpProductTags.setRetailerId(appRetailerId);
}]);
```

### Exported Constants

* `PRODUCT_TAG_TYPES` An object of the product tags types.
* `PRODUCT_TAGS_CACHE_NAME` The name of the `sp-local-cache` for the product tags.

### `SpProductTags` Service

* `setRetailerId(retailerId) => void` Saves the retailer id on the service to be able to GET product tags.
* `getRetailerId() => Promise<number>` Returns a promise which will be resolved when the retailer id will be set.
* `isProductTagRelationActive(productTagRelation) => boolean` Returns whether the given product tag relation is active. The product tag relation is the data on `esProduct.productTagsData`.

### Exported Filters

* `productTag` Same as `productTagId | spLocalCache:PRODUCT_TAGS_CACHE_NAME`.
* `productMainTag` Converts a product to its main product tag. The main product tag is the first active product tag with an icon on it.

### Build
* `npm i` to install dependencies
* `npm run build` to build new version to /dist folder